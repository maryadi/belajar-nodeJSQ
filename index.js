var http = require('http');
var url = require('url');
var router = require('routes')();
var view = require('swig');
var mysql = require('mysql');
var qString = require('querystring');
var connection = mysql.createConnection({
	host 	 : "localhost",
	port 	 : 3306,
	database : "nodejs",
	user 	 :"root",
	password : ""
});
router.addRoute('/',function(req, res){
	connection.query("select * from mahasiswa", function(err, rows, field){
		if (err) throw err;
		var html = view.compileFile('./template/index.html')({
			title : "Data Mahasiswa",
			data : rows		
		});

	res.writeHead(200,{"Content-type" : "text/html"});
	res.end(html);
	});
});


router.addRoute('/insert',function(req, res){
	if (req.method.toUpperCase() == "POST") {
		var data_post = "";
		req.on('data', function(chuncks){
			data_post += chuncks;
		});

		req.on('end', function(chuncks){
			data_post = qString.parse(data_post);
			connection.query("insert into mahasiswa set ? ", data_post, 
				function(err, field){ 
					if(err) throw err;
					res.writeHead(302,{"Location" : "/"});
					res.end();	
				}	
			);
		});
	}else{
		var html = view.compileFile('./template/form.html')();
		res.writeHead(200,{"Content-type" : "text/html"});
		res.end(html);	
	}
});

router.addRoute('/update/:id',function(req, res){
	connection.query("select * from mahasiswa where ? ",
				{ no_induk : this.params.id }, 
				function(err, rows,field){
					if (rows.length) {
						var data = rows[0];
						if (req.method.toUpperCase() == "POST") {
							var data_post = "";
							req.on('data', function(chuncks){
								data_post += chuncks;
							});

							req.on('end', function(chuncks){
								data_post = qString.parse(data_post);
								connection.query("update mahasiswa set ? where ?",[
									data_post,
									{ no_induk : data.no_induk }
									],  
									function(err, fields){ 
										if(err) throw err;
										res.writeHead(302,{"Location" : "/"});
										res.end();	
									}	
								);
							});	
						}else{
							var html = view.compileFile('./template/form_update.html')({
								data : data
							});
							res.writeHead(200,{"Content-type" : "text/html"});
							res.end(html);	
						}
					}else{
						var html = view.compileFile('./template/notfound.html')();
						res.writeHead(200,{"Content-type" : "text/html"});
						res.end(html);		
					}
				}	
			);
});
router.addRoute('/delete/:id',function(req, res){
	connection.query("delete from mahasiswa where ?", 
		{ no_induk : this.params.id },
		function(err,fields){
			if (err) throw err;

			res.writeHead(302,{"Location" : "/"});
			res.end();
		}
	);
});
router.addRoute('/contact',function(req, res){
	var html = view.compileFile('./template/contact.html')();
	res.writeHead(200,{"Content-type" : "text/html"});
	res.end(html);
});

http.createServer(function (req, res) {
	var path = url.parse(req.url).pathname;
	var match = router.match(path);
	if (match) {
		match.fn(req, res);
	}else{
		var html = view.compileFile('./template/notfound.html')();
		res.writeHead(200,{"Content-type" : "text/html"});
		res.end(html);		
	} 
   //write a response to the client
  
}).listen(8888);
console.log("Sambung");