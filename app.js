var http = require("http"); 
var fs = require("fs");
var url = require("url");
http.createServer(function (req, res) {

	if (req.url != "/favicon.ico") {
		var access = url.parse(req.url); 
		var file = "";
		if (access.pathname == "/") {
			file = "../template/index.html";
		}else if (access.pathname == '/contact') {
			file = '../template/contact.html';
		}else{
			file = '../template/notfound.html';
		}
		res.writeHead(200, {"Content-type" : "text/html"});
		fs.createReadStream('./template/'+file).pipe(res);
	}
}).listen(8888);

console.log("server jalan ..");